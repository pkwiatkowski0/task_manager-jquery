var application = (function () {
    function start() {
        console.log("Start");
        $(".demoBox  .x").click(closeDemoBox);
    }
    $("[class='confirmButton']").on("click", addTask);
    /*te funkcje klikajace robia to samo tyle ze .on wskazuje co dokladnie ma byc klikniete */
    function closeDemoBox() {
        $(".demoBox").slideUp("slow");
    }

    function addTask() {
        alert("addTask");
        var nameFieldValue = $("[placeholder = 'Name']").val();
        var descriptionFieldValue = $("[placeholder = 'Description']").val();
        var taskListFieldValue = $("[name='taskList]").val();
        if (nameFieldValue.length > 2 && nameFieldValue.length < 22 && descriptionFieldValue.length > 2 && descriptionFieldValue.length > 22 && taskListFieldValue.length !== 0) {
            console.log("Poprawne dane!");
            var newLine = $("<p>", {});
            $(newLine).html(nameFieldValue + " " + descriptionFieldValue + " " + taskListFieldValue);
            $(".taskList").append(newLine);
        }
        else {
            console.log("Niepoprawne dane!");
        }
    }
    
    function newTask() {
        var desc = $("#newTaskForm [name='taskDesc']");
        var name = $("#newTaskForm [name='taskName']");
        var category = $("#newTaskForm select option:selected")
    
        var newTask = {
            name: name.val(),
            desc: desc.val(),
            isDone: false;
            
        }
    }
    
    
    return {
        start: start
    };
    /* Podpiac 2 pluginy: date z kalendarzykiem rozwijanym i alertify (ładnie wyskakujace okienka) 
    sprawdzic w prezentacji */
})();